#####
# Lambda functions
#

resource "aws_lambda_function" "sns_to_slack" {
  filename         = "${path.module}/lambda/sns-to-slack.zip"
  function_name    = var.lambda_function_name
  role             = aws_iam_role.lambda_sns_to_slack.arn
  handler          = "lambda_function.lambda_handler"
  source_code_hash = filebase64sha256("${path.module}/lambda/sns-to-slack.zip")
  runtime          = "python3.12"
  description      = "Managed by Terraform"
  timeout          = "60"

  environment {
    variables = {
      WEBHOOK_URL      = var.slack_webhook_url
      CHANNEL_MAP      = base64encode(var.slack_channel_map)
      DEFAULT_USERNAME = var.default_username
      DEFAULT_CHANNEL  = var.default_channel
      DEFAULT_EMOJI    = var.default_emoji
      ENVIRONMENT      = var.environment
    }
  }

  depends_on = [aws_cloudwatch_log_group.sns_to_slack]
}

resource "aws_cloudwatch_log_group" "sns_to_slack" {
  name = "/aws/lambda/${var.lambda_function_name}"

  retention_in_days = 14
}
